

const devServerProperties = {
  vendors: ['src/vendor.js'],
  apiUrl: '/mes-starter-service',

  proxy: {
	'/bid': {
		target: 'httep://localhost:8080',
		secure: false,
	},
	'/user': {
		target: 'httep://localhost:8080',
		secure: false,
	},
	'/item': {
		target: 'httep://localhost:8080',
		secure: false,
	}
  },
};

module.exports = devServerProperties;
