# react-starter-front

#### front 실행
* `npm install yarn -g` : yarn이 설치 되어 있지 않다면, 
  * unable to verify the first certificate 오류 발생시에는 `npm config set registry http://registry.npmjs.org/ --global`
* `yarn` : node_modules 디렉토리 생성 (npm 사용 시엔 `npm install`)
* `yarn start` : 로컬 서버 실행 (`npm start`)

#### front 빌드
* `yarn build_app` : /dist/app에 빌드 결과물 생성 (`npm run build_app`)

#### npm install 시 에러
* UNABLE_TO_VERIFY_LEAF_SIGNATURE : `npm config set strict-ssl false`
* unable to verify the first certificate : `yarn config set "strict-ssl" false -g`

#### 로컬 서버 포트 변경
* package.json -> scripts -> start 명령에 --port 옵션 수정

#### 로컬환경에서 서버 REST API 적용
* /config/devServer.properties.js의 proxy 설정 수정
