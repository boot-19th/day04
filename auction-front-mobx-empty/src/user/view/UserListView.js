
import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider } from 'semantic-ui-react';

import UserListTableView from './UserListTableView';
import UserEditFormView from './UserEditFormView';

class UserListView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                    User List
                    <Button floated='right' primary >New</Button>
                </Header>
                <Divider/>
                <UserEditFormView
                />
                <UserListTableView
                />
            </Container>
        );
    }
}

export default UserListView;