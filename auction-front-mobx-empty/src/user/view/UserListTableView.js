
import React, { PureComponent } from 'react';
import { Table, Button } from 'semantic-ui-react';

class UserListTableView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Role</Table.HeaderCell>
                        <Table.HeaderCell>Item</Table.HeaderCell>
                        <Table.HeaderCell>Bid</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    <Table.Row >
                        <Table.Cell style={{cursor: 'pointer'}}
                        >
                        </Table.Cell>
                        <Table.Cell></Table.Cell>
                        <Table.Cell textAlign='center'>
                                <Button >Move</Button>
                        </Table.Cell>
                        <Table.Cell textAlign='center'>
                                <Button >Move</Button>
                        </Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>No Users</Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        );
    }
}

export default UserListTableView;
