
import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';

class UserEditFormView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Name' placeholder='Name' 
                    />
                </Form.Group>
                <Form.Group inline>
                    <label>Role</label>
                    <Form.Checkbox
                        label='Seller'
                        value='Seller'
                    />
                    <Form.Checkbox
                        label='Bidder'
                        value='Bidder'
                    />
                </Form.Group>
                <Button primary >Save</Button> }
                <Button secondary >Cancel</Button>
            </Form>
        )
    }
}

export default UserEditFormView;