
import React, { Fragment } from 'react';
import { BrowserRouter, Switch, Redirect, Route } from 'react-router-dom';

function Routes() {
    return (
        <BrowserRouter basename="/">
            <Switch>
                <Redirect exact from="/" to="/front/user"/>
                <Route path="/front" component={({match}) =>
                    <React.Fragment>
                    </React.Fragment>
                }/>
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
