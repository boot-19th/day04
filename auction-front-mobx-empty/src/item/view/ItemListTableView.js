
import React, { PureComponent } from 'react';
import { Table } from 'semantic-ui-react';

class ItemListTableView extends PureComponent {
    //
    render() {
        //
        const {

        } = this.props;
        return (
            <Table celled>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Name</Table.HeaderCell>
                        <Table.HeaderCell>Start Price</Table.HeaderCell>
                        <Table.HeaderCell>Price Step</Table.HeaderCell>
                        <Table.HeaderCell>Current Price</Table.HeaderCell>
                        <Table.HeaderCell>Seller</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    <Table.Row >
                        <Table.Cell style={{cursor: 'pointer'}}
                        >
                        </Table.Cell>
                        <Table.Cell></Table.Cell>
                        <Table.Cell></Table.Cell>
                        <Table.Cell></Table.Cell>
                        <Table.Cell></Table.Cell>
                    </Table.Row>
                    <Table.Row>
                        <Table.Cell>No Items</Table.Cell>
                    </Table.Row>
                </Table.Body>
            </Table>
        );
    }
}

export default ItemListTableView;