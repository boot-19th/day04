
import React, { PureComponent } from 'react';
import { Container, Header, Button, Divider } from 'semantic-ui-react';

import ItemListTableView from './ItemListTableView';
import ItemEditFormView from './ItemEditFormView';

class ItemListView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Container style={{marginTop: 50}}>
                <Header as='h2'>
                     Item List
                    <Button floated='right' primary >New</Button>
                </Header>
                <Divider/>
                <ItemEditFormView
                />
                <Divider/>
                <ItemListTableView
                />
            </Container>
        );
    }
}

export default ItemListView;