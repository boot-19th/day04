
import React, { PureComponent } from 'react';
import { Header, Divider } from 'semantic-ui-react';

import ItemDetailItemView from './ItemDetailItemView';

class ItemDetailView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <div>
                <Header as='h2'>
                    's Item
                </Header>
                <Divider/>
                <ItemDetailItemView 
                />
            </div>
        );
    }
}

export default ItemDetailView;