
import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';

class ItemEditFormView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;

        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Name' placeholder='Name' 
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.TextArea label='Description' placeholder='Description' 
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Start Price' placeholder='Start Price' 
                    />
                    <Form.Input fluid label='Price Step' placeholder='Price Step' 
                    />
                </Form.Group>
                <Button primary >Save</Button>
                <Button secondary >Cancel</Button>
            </Form>
        )
    }
}

export default ItemEditFormView;