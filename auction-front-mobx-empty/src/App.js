import { Provider } from 'mobx-react';

import Routes from './Routes';

import React, { Component } from 'react';

class App extends Component {
	render(){
		return (
			<Provider
			>

				<Routes/>
			</Provider>

		);
	}

}
export default App;
