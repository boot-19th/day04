
import React, { PureComponent } from 'react';
import { Form, Button } from 'semantic-ui-react';

class BidEditFormView extends PureComponent {
    //
    render() {
        //
        const {
        } = this.props;
        return (
            <Form>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Bidder Id' placeholder='Bidder Id' 
                    />
                    <Form.Input fluid label='Bidder Name' placeholder='Bidder Name' 
                    />
                </Form.Group>
                <Form.Group widths='equal'>
                    <Form.Input fluid label='Price' placeholder='Price' 
                    />
                </Form.Group>
                <Button primary >Bid</Button>
                <Button secondary >Cancel</Button>
            </Form>
        )
    }
}

export default BidEditFormView;